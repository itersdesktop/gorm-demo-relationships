package com.itersdesktop.javatechs.grails.gorm

class SchoolTeacher implements Serializable {
    String name

    static belongsTo = [schoolClass: SchoolClass]

    static constraints = {
    }
}
