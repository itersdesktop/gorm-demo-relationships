package com.itersdesktop.javatechs.grails.gorm

class Pupil implements Serializable {
    String name
    static belongsTo = [schoolClass: SchoolClass]

    static constraints = {
    }
}
