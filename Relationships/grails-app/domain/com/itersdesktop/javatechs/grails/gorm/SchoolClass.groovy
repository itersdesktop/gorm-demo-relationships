package com.itersdesktop.javatechs.grails.gorm

class SchoolClass implements Serializable{
    // for example: Class 1 or Owl class
    String name
    // year 1, year 2, etc.
    Integer year
    // for example: 2018-2019
    String schoolYear
    // there are at least two teachers: main and assistant
    static hasMany = [teachers: SchoolTeacher, pupils: Pupil]

    static mapping = {
        teachers cascade: 'all-delete-orphan'
        pupils cascade: 'all-delete-orphan'
    }

    static constraints = {
    }
}
