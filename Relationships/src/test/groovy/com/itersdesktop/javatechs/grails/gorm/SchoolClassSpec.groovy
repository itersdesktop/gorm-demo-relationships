package com.itersdesktop.javatechs.grails.gorm

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class SchoolClassSpec extends Specification implements DomainUnitTest<SchoolClass> {
    SchoolClass schoolClass
    Pupil p

    void setupData() {
        schoolClass = new SchoolClass(name: "Owl class", year: 2, schoolYear: "2017-2018")
        p = new Pupil(name: "Tony Nguyen")
        schoolClass.addToPupils(p)
    }

    void "test SchoolClass domain"() {
        given: "let's create a school class and a pupil"
        setupData()
        when: "save the school class"
        schoolClass.save()
        then: "we also save the pupil because we set cascade on SchoolClass"
        1 == SchoolClass.count
        1 == Pupil.count
        when: "get the school class and the pupil back"
        p = Pupil.get(1)
        schoolClass = SchoolClass.get(1)
        then: "we hope the returned values match with the inputs"
        p.name == "Tony Nguyen"
        schoolClass.name == "Owl class"
        schoolClass.schoolYear == "2017-2018"
        when: "let's delete the school class"
        schoolClass.delete(flush: true, failOnError: true)
        then: "we hope the pupil will be deleted as well"
        0 == SchoolClass.count
        1 == Pupil.count
    }
}
